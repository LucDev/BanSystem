package me.lucdev.opensource.bansystem.commands;

import me.lucdev.opensource.bansystem.BanSystemPlugin;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandKick extends Command {

    public CommandKick() {
        super("kick", "ban.commands.kick.use", "");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length >= 2) {
            ProxiedPlayer proxiedTarget = ProxyServer.getInstance().getPlayer(args[1]);
            if (proxiedTarget != null) {
                if (proxiedTarget.hasPermission("ban.commands.kick.bypass")) {
                    StringBuilder kickReasonStringBuilder = new StringBuilder();

                    for (int i = 1; i < args.length; i++) {
                        kickReasonStringBuilder.append(args[i]).append(" ");
                    }

                    proxiedTarget.disconnect("§cYou got kicked! \n"
                            + " \n"
                            + "§cReason §8» §e" + kickReasonStringBuilder + "\n"
                            + "§cKicked by §8» §e" + sender.getName());
                } else {
                    sender.sendMessage(BanSystemPlugin.getInstance().getBanPluginPrefix() + "§cYou can not kick this player...");
                }
            } else {
                sender.sendMessage(BanSystemPlugin.getInstance().getBanPluginPrefix() + "§cThe player " + args[1] + " is not online!");
            }
        } else {
            sender.sendMessage(BanSystemPlugin.getInstance().getBanPluginPrefix() + "§7cPlease use /kick <name> <reason>");
        }
    }
}
