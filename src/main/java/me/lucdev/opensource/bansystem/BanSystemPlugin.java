package me.lucdev.opensource.bansystem;

import lombok.Getter;
import me.lucdev.opensource.bansystem.commands.CommandKick;
import me.lucdev.opensource.bansystem.database.MySQL;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

public final class BanSystemPlugin extends Plugin {

    @Getter
    private static BanSystemPlugin instance;
    @Getter
    private final String banPluginPrefix = "§8[§cBan§8] ";
    @Getter
    private final String debugPrefix = "§8[§bDEBUG§8] ";

    @Override
    public void onLoad() {
        getLogger().info(getBanPluginPrefix() + "§7The plugin is §eloading§7...");
    }

    @Override
    public void onEnable() {
        instance = this;

        init();

        getLogger().info(getBanPluginPrefix() + "§7The plugin was successful §aenabled§7...");
    }

    @Override
    public void onDisable() {
        instance = null;

        MySQL.getInstance().closeConnection();

        getLogger().info(getBanPluginPrefix() + "§7The plugin was successful §cdisabled§7...");
    }

    private void init() {
        MySQL.getInstance().openConnection();
        handleRegistration();

        getLogger().info(getBanPluginPrefix() + "§7The plugin was successful §aloading§7...");
    }

    private void handleRegistration() {
        PluginManager pluginManager = getProxy().getPluginManager();

        pluginManager.registerCommand(this, new CommandKick());
    }

}
