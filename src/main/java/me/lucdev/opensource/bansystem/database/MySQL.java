package me.lucdev.opensource.bansystem.database;

import lombok.Getter;
import me.lucdev.opensource.bansystem.BanSystemPlugin;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

    @Getter
    private static MySQL instance;
    private Connection connection;

    public void openConnection() {
        if (!(isConnected())) {
            try {
                connection = DriverManager.getConnection("jdbc:mysql://"
                        + "localhost"
                        + 3360
                        + "/bansystem"
                        , "root"
                        , "");
            } catch (SQLException sqlException) {
                BanSystemPlugin.getInstance().getLogger().info(BanSystemPlugin.getInstance().getDebugPrefix() + "§cError: We can not connect to the database.");
                sqlException.printStackTrace();
            }
        } else {
            BanSystemPlugin.getInstance().getLogger().info(BanSystemPlugin.getInstance().getDebugPrefix() + "§cError: The database is already connected.");
        }
    }

    public void closeConnection() {
        if (isConnected()) {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                BanSystemPlugin.getInstance().getLogger().info(BanSystemPlugin.getInstance().getDebugPrefix() + "§cError: We can not close the connection to the database.");
                sqlException.printStackTrace();
            }
        } else {
            BanSystemPlugin.getInstance().getLogger().info(BanSystemPlugin.getInstance().getDebugPrefix() + "§cError: The database is disconnected.");
        }
    }

    private boolean isConnected() {
        return connection != null;
    }

}
